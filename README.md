
# Generic Raw Image Processor

## Description
The Generic Raw Image Processor is a versatile Python utility designed for handling raw image files in various formats and sizes. It provides robust tools for reading, processing, and saving raw images, catering especially to those working with custom image data formats in scientific and industrial applications.



## Usage
Simple usage example:
```python
from generic_raw import GenericRaw

# Initialize the processor with an image
raw_img = GenericRaw('path/to/your/image.raw', 256, 256, np.uint32)

# Process the image
raw_img.cast_to_double()

# Save the processed image
raw_img.save_to_file('path/to/your/output_image.raw')
```


## Contributing
Contributions are welcome! Please fork the repository, make your changes, and submit a pull request. For major changes, please open an issue first to discuss what you would like to change.


## Project Status
Development is active. 