# -*- coding: utf-8 -*-
"""
This module tests the generic_raw module


Created on Sat Apr 21 15:43:00 2024

@author: mgaede
"""

import numpy as np
import unittest
import os


from generic_raw import GenericRaw


class TestGenericRaw(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Set up paths for the test files
        cls.test_image_path = 'test_raw_image.raw'
        cls.output_file_path = 'output_image.raw'

    def test_read_raw_file(self):
        # Initialize the object with test parameters
        raw = GenericRaw(self.test_image_path, 256, 256,
                         np.uint32, byte_order='LSB', header_offset=16)
        # Test the image data shape and type correctness
        self.assertEqual(raw.img_data.shape, (65536, ))
        self.assertEqual(raw.img_data.dtype, np.uint32)

    def test_cast_to_double(self):
        # Test casting to double
        raw = GenericRaw(self.test_image_path, 256, 256,
                         np.uint32, byte_order='LSB', header_offset=16)
        raw.cast_to_double()
        self.assertEqual(raw.img_data.dtype, np.float64)

    def test_cast_to_uint32(self):
        # Test casting back to uint32
        raw = GenericRaw(self.test_image_path, 256, 256,
                         np.uint32, byte_order='LSB', header_offset=16)
        raw.cast_to_double()  # First cast to double
        raw.cast_to_uint32()  # Then cast back to uint32
        self.assertEqual(raw.img_data.dtype, np.uint32)

    def test_save_to_file(self):
        # Test saving to file
        raw = GenericRaw(self.test_image_path, 256, 256,
                         np.uint32, byte_order='LSB', header_offset=16)
        raw.save_to_file(self.output_file_path)
        # Check if file exists
        self.assertTrue(os.path.exists(self.output_file_path))

    @classmethod
    def tearDownClass(cls):
        # Clean up (delete the created files)
        if os.path.exists(cls.output_file_path):
            os.remove(cls.output_file_path)


if __name__ == '__main__':
    unittest.main()
