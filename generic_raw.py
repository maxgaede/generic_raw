# -*- coding: utf-8 -*-
"""
This module defines a class for handling raw image data files.
It allows for reading raw binary image data with configurable 
headers, footers, and data types.


Created on Thu Dec 15 21:19:42 2022

@author: mgaede
"""
import numpy as np
import os


class GenericRaw:
    """
    A class to read and handle raw image data.

    Attributes:
        img_path (str): Path to the image file.
        img_height (int): Height of the image in pixels.
        img_width (int): Width of the image in pixels.
        pixel_count (int): Total number of pixels in the image.
        data_type (np.dtype): Data type of the image pixels.
        byte_order (str): Byte order of the data, 'LSB' or 'MSB'.
        header_offset (int): Byte offset at which the image data starts.
        header (bytes): Header data of the image file.
        img_data (np.ndarray): The image data as a numpy array.

    Parameters:
        img_path (str): Path to the image file.
        img_height (int): Height of the image in pixels.
        img_width (int): Width of the image in pixels.
        data_type (np.dtype): Data type of the image pixels.
        byte_order (str): Byte order of the data, defaults to 'LSB'.
        header_offset (int): Byte offset at which the image data starts,defaults to 0.
        footer_offset (int): Byte offset at which the image data ends, defaults to 0.
    """

    def __init__(self, img_path: str,
                 img_height: int,
                 img_width: int,
                 data_type: np.dtype,
                 byte_order: str = "LSB",
                 header_offset: int = 0,
                 footer_offset: int = 0):

        self.img_path = img_path
        self.img_height = img_height
        self.img_width = img_width
        self.pixel_count = img_height*img_width
        self.data_type = data_type
        self.byte_order = byte_order
        self.header_offset = header_offset
        self.header = None
        self.img_data = None
        self._read_raw_file()

    def _read_raw_file(self):
        '''
        Reads image file, extracts header if exists, and loads image data into a numpy array based on provided attributes.

        Returns
        -------
        None.

        '''
        with open(self.img_path, 'rb') as img_file:
            if self.header_offset != 0:
                self.header = img_file.read(self.header_offset)
            self.img_data = np.fromfile(img_file,
                                        dtype=self.data_type,
                                        count=self.pixel_count,
                                        offset=self.header_offset)

    def cast_to_double(self):
        '''
        Converts the image data array to a double-precision float format.

        Returns
        -------
        None.

        '''
        self.img_data = np.double(self.img_data)

    def cast_to_uint32(self):
        '''
        Converts the image data array to a 32-bit unsigned integer format.

        Returns
        -------
        None.

        '''
        self.img_data = self.img_data.astype(np.uint32)

    def cast_to_uint16(self):
        '''
        Converts the image data array to a 16-bit unsigned integer format.

        Returns
        -------
        None.

        '''
        self.img_data = self.img_data.astype(np.uint16)

    def decompand(self, pwl_from: np.array, pwl_to: np.array):
        '''
        Applies a piecewise linear transformation to the image data using provided mapping arrays.

        Parameters
        ----------
        pwl_from : np.array
            Array of original pixel values used as knee points for the transformation.
        pwl_to : np.array
            Array of target pixel values corresponding to the knee points in `pwl_from`.

        Returns
        -------
        None.

        '''
        self.img_data = np.interp(self.img_data, pwl_from, pwl_to)

    def subtract_pedestal(self, pedestal: int):
        '''
        Subtracts a constant value (pedestal) from each pixel in the image data.

        Parameters
        ----------
        pedestal : int
            The pedestal value to subtract from all pixel values.

        Returns
        -------
        None.

        '''
        self.img_data = self.img_data - pedestal

    def devide_by_transaction_factor(self, transaction_factor: float):
        '''
        Divides each pixel in the image data by a given transaction factor to normalize or scale the pixel values.

        Parameters
        ----------
        transaction_factor : float
            The factor by which each pixel value is divided.

        Returns
        -------
        None.

        '''
        self.img_data = self.img_data / transaction_factor

    def save_to_file(self, output_file_path: str):
        '''
        Saves the processed image data to a file.

        Parameters
        ----------
        output_file_path : str
            The path to the file where the image data should be saved.

        Returns
        -------
        None.

        '''
        self.img_data.tofile(output_file_path)


# Testclient
if __name__ == "__main__":

    pwl_from = np.array([0, 256, 512, 620, 740, 1024, 1270, 1500,
                        2000, 2170, 2300, 2430, 2670, 2950, 3210, 3500, 3720, 3968])
    pwl_to = np.array([0, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536,
                      131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777215])
    pedestal = 512
    transaction_factor = 1.234

    dir_path = "ENTER PATH TO EXAMPLE RAW FILE DIRECTORY"
    list_of_files = os.listdir(dir_path)
    for count, filename in enumerate(list_of_files):
        path = os.path.join(dir_path, filename)
        # Raw Conversion Process
        # define your image dimensions and datatype
        raw_img = GenericRaw(path, img_height=128, img_width=128,
                             data_type=np.uint8, byte_order="LSB")
        raw_img.cast_to_double()
        raw_img.decompand(pwl_from, pwl_to)
        raw_img.subtract_pedestal(pedestal)
        raw_img.devide_by_transaction_factor(transaction_factor)
        raw_img.cast_to_uint32()
        raw_img.save_to_file("ENTER EXAMPLE FILE NAME HERE")
